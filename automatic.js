var log = require('./app/logging.js');
var request = require('request');

// Make sure we're using a supported node.js version
if(process.versions && process.versions.node) {
    var parts = process.versions.node.split('.');
    if(parts[0] <= 0 && parts[1] < 12) {
        console.log("FATAL ERROR: backpack.tf Automatic requires node.js version 0.12.0 or later.");
        process.exit(1);
    }
}

// The following log statement will initialize the SteamClient since timestamps are prefixed with Steam usernames
log.info("backpack.tf Automatic v%s starting", require('./package.json').version);

// Check if we're up to date
request.get({
    "uri": "https://bitbucket.org/srabouin/backpack.tf-automatic/raw/master/package.json",
    "json": true
}, function(err, response, body) {
    if(err || response.statusCode != 200) {
        log.warn("Cannot check for updates: " + (err ? err.message : "HTTP error " + response.statusCode));
        return;
    }

    if(!body.version) {
        log.warn("Cannot check for updates: malformed response");
        return;
    }

    var current = require('./package.json').version.split('.');
    var latest = body.version.split('.');

    for(var i = 0; i < 3; i++) {
        if(current[i] < latest[i]) {
            log.info("===================================================");
            log.info("Update available! Current: v%s, Latest: v%s", require('./package.json').version, body.version);
            log.info("===================================================");
            return;
        }
    }
});
