var fs = require('fs');
var log = require('./logging.js');

const CONFIG_FILENAME = 'config.json';

var g_Config;

exports.get = function() {
	g_Config.steam = g_Config.steam || {"sentries": {}, "identitySecret": {}};
	g_Config.tokens = g_Config.tokens || {};
	return g_Config;
};

exports.write = function(config) {
	g_Config = config;
	fs.writeFileSync(CONFIG_FILENAME, JSON.stringify(config, null, "\t"));
};

if (fs.existsSync(CONFIG_FILENAME)) {
	loadConfig();
} else {
	exports.write({
		"dateFormat": "HH:mm:ss",
		"acceptOverpay": false,
		"acceptGifts": false,
		"declineBanned": true,
		"acceptEscrow": false,
		"acceptedKeys": [
			"Mann Co. Supply Crate Key"
		],
		"logs": {
			"console": {
				"level": "verbose"
			},
			"file": {
				"disabled": false,
				"level": "info"
			},
			"trade": {
				"disabled": false
			}
		},
		"steam": {
			"sentries": {},
			"identitySecret": {}
		},
		"tokens": {}
	});
}

function loadConfig() {
	try {
		g_Config = JSON.parse(fs.readFileSync(CONFIG_FILENAME));
	} catch(e) {
		log.warn("Cannot load " + CONFIG_FILENAME + ". " + e.toString());
	} finally {
		log.info(CONFIG_FILENAME + " " + (g_Config ? "reloaded" : "loaded") + " successfully.");
	}
}
